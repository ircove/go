﻿--判断是否可以投票
if redis.call("HINCRBY","app_checksendcode"..@today,@mobile,1)  > 10  then
	return -3;
end
if redis.call("HINCRBY","app_checksendcode"..@today,@deviceid,1) > 10 then
   return -2;
end
--企业投票总数限制
if redis.call("HINCRBY","app_checksendcode"..@today,@ip,1) > 30 then
	return -1
end
return 1
