package irc

type Session struct {
	DeviceId string
	VToken    string
	AppId    int
	UserIsAuth   bool
	UserName     string
	UserID       int64
	AuthType string
}

