package irc

import (
	"testing"
	"os"
	"encoding/json"
	"fmt"
)

type News struct {
	ID    int    `json:"id"`
	Title string `json:"title"`
	Content string `json:"content"`
}

func  TestMessage_Encode(t *testing.T) {

	new :=new(News)
	new.ID=1
	new.Title="Go语言中的 Array, Slice和 Map"
	new.Content=`介绍
曾经学习python的时候，记得书上说 dict 是 python的 horsepower（动力）。然后,Slice 和 Map 又何尝不是 golang 的 workhorse 呢？

Array 是值类型，Slice 和 Map 是引用类型。他们是有很大区别的，尤其是在参数传递的时候。
另外，Slice 和 Map 的变量 仅仅声明是不行的，必须还要分配空间（也就是初始化，initialization） 才可以使用。
第三，Slice 和 Map 这些引用变量 的 内存分配，不需要你操心，因为 golang 是存在 gc 机制的（垃圾回收机制）`

   bytes,err := json.Marshal(new)

	if err !=nil {
		println(t,"序列化出错啦！")
		return
	}



	head := &Header{
		HasMessageLen:true,
		HasMessageID:true,
		MessageLen:256,
		MessageID:1,
		MessageType:MessageType_RPC,
		MessageFormat:MessageFormat_JSON,
	}
	message :=&Message{
		Header:head,
		Data:bytes,
	}
	//message.Header=head

    message.Header.SetMessageLen(len(bytes))
	message.Header.SetMessageID(1)


	f,err:= os.Create("D:\\index.data")
	if err !=nil {
		t.Fatal(err)
		return
	}
	defer f.Close()


	err = message.Encode(f)
	if err !=nil {
		t.Fatal(err)
	}
	println(t,"ok")
}

func TestMessage_Decode(t *testing.T) {
	message := new(Message)

	f,err:= os.Open("D:\\index.data")
	if err !=nil {
		t.Fatal(err)
		return
	}
	defer f.Close()
	err = message.Decode(f)
	if err !=nil {
		t.Fatal(err)
	}

    bytes:=message.Data;
	if bytes ==nil {
		println(t,"message data is empyt!")
		return
	}
	var news =new(News)
	if err = json.Unmarshal(bytes,news);err!=nil{
		println(t,"json Unmarshal error!"+err.Error())
		return
	}
	fmt.Printf("id:%d\n title:%s\n content:%s",news.ID,news.Title,news.Content)
	println(t,message.Header.MessageID)
	println(t,message.Header.MessageType)
	println(t,message.Header.MessageFormat)

}