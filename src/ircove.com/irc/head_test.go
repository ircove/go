package irc

import (
	"testing"
	"os"
)

func TestHeader_Encode(t *testing.T) {

	head := &Header{
		HasMessageLen:false,
		HasMessageID:false,
		MessageLen:0,
		MessageID:0,
		MessageType:MessageType_HeartBeat,
		MessageFormat:MessageFormat_JSON,

	}
	f,err:= os.Create("D:\\index.data")
	if err !=nil {
		t.Fatal(err)
		return
	}
	defer f.Close()


	err = head.Encode(f)
	if err !=nil {
		t.Fatal(err)
	}
	println(t,"ok")

}

func TestHeader_Decode(t *testing.T) {
	head := new(Header)

	f,err:= os.Open("D:\\index.data")
	if err !=nil {
		t.Fatal(err)
		return
	}
	defer f.Close()


	err = head.Decode(f)
	if err !=nil {
		t.Fatal(err)
	}
	println(t,head)
}