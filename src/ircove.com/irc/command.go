package irc

import (
	. "ircove.com/common"
	"fmt"
	"sync/atomic"
)

var commands []func(*VFContext) *VFResult = make([]func(*VFContext) *VFResult, 256, 512)

func init() {
	commands[0] = HelloWorldCommand
	commands[1] = HelloWorldCommand
}

type VFContext struct {
	Client  *Client
	Request *Message
	Args    map[string]string
}

func FindCommand(fid uint16) func(*VFContext) *VFResult {
	if int(fid) < len(commands) {
		return commands[fid]
	}
	return nil
}

func RegisterCommand(fid uint16,fun func(*VFContext) *VFResult)  {
	  commands[fid] = fun
}
var max uint64

func HelloWorldCommand(context *VFContext) *VFResult {
	return NewVFResult(fmt.Sprintf("helloword!%d",atomic.AddUint64(&max,1)))
}
