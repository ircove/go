package common
type VFResult struct {
	Msg string             `json:"msg"`
	Value interface{}      `json:"data"`
	Code int               `json:"code"`
}

func(result *VFResult) Success() bool {
    return result.Code== Success.Code
}

func NewVFResult(Value interface{})  *VFResult {
	 r :=new(VFResult)
	 r.Code=Success.Code
	 r.Msg=Success.Msg
	 r.Value=Value
	 return r
}
func NewVFResult_VFcode(code *VFCode)  *VFResult {
	r :=new(VFResult)
	r.Code=code.Code
	r.Msg=code.Msg
	r.Value=code.Name
	return r
}