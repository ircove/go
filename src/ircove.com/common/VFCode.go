package common

import (
	"fmt"
)

/*
  返回值Code类
*/
type VFCode struct {
	Code int     `json:"code"`
	Name  string `json:"name"`
	Msg  string  `json:"msg"`
}

var Success = &VFCode{
	Code: 200, Name: "Success", Msg: "操作成功",
}
var Fail = &VFCode{
	Code: 510, Name: "Fail", Msg: "操作失败",
}
var ServiceUnavailable = &VFCode{
	Code: 500, Name: "ServiceUnavailable", Msg: "服务暂时不可用",
}
var OperationalError = &VFCode{
	Code: 512, Name: "OperationalError", Msg: "操作错误",
}
var NetWorkError = &VFCode{
	Code: 513, Name: "NewWorkError", Msg: "网络出错",
}
var NetWorkTimeout = &VFCode{
	Code: 514, Name: "NewWorkTimeout", Msg: "网络超时",
}

var SerializeError = &VFCode{
	Code: 520, Name: "SerializeError", Msg: "序列化失败",
}
var CancelError = &VFCode{
	Code: 333, Name: "CancelError", Msg: "操作已取消",
}
var ObjecctNotFound = &VFCode{
	Code: 404, Name: "ObjecctNotFound", Msg: "对象未找到",
}
var SesstionInvaild = &VFCode{
	Code: 12, Name: "SesstionInvaild", Msg: "sessiontoken已经失效",
}
var InvalidSignature = &VFCode{
	Code: 96, Name: "InvalidSignature", Msg: "签名不正确",
}
var MissingSignature = &VFCode{
	Code: 97, Name: "Success", Msg: "需要签名，但是没有传递这个参数",
}
var InternalError = &VFCode{
	Code: 500, Name: "Success", Msg: "服务器内部错误",
}
var Permissions = &VFCode{
	Code: 99, Name: "Success", Msg: "用户未登录或权限不足",
}

var VFException = &VFCode{
	Code: 200, Name: "Success", Msg: "程序发生了异常",
}
var ErrorArgument = &VFCode{
	Code: 111, Name: "ErrorArgument", Msg: "参数不正确",
}

func (this *VFCode) Success() bool {
	if this == nil {
		return  true
	}
	return this.Code == 200
}
func (this *VFCode) Clone(msg string) *VFCode  {
	return &VFCode{
		Code:this.Code, Name: this.Name, Msg: msg,
	}
}
func (this *VFCode) Error() string {
	return fmt.Sprintf(`{"Code":%d,"name":"%s","Message":"%s"}`, this.Code, this.Name, this.Msg)
}

func IsNewWorkError(err error)  bool {

	if err ==nil {
		return false
	}

	this,ok:=err.(*VFCode)
	if !ok {
		return false
	}
	if this.Code== NetWorkTimeout.Code {
		return  true
	}
	if this.Code==NetWorkError.Code {
       return  true
	}
	return  false
}