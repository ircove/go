package common

import "strconv"

type XMessage struct {
	After_Open string `json:"after_open"`
	Title   string `json:"title"`
	Text    string `json:"text"`
	Image   string `json:"image"`
	JumpType int `json:"jumptype"`
	MessageType int `json:"messagetype"`
	NewsId int `json:"newsid"`
	Url string `json:"url"`
	ExpireTime string `json:"expire_time"`
	DeviceTokens string `json:"device_tokens"`
}

func  XMessage_MapFrom(arg map[string]string) *XMessage  {

	this :=new(XMessage)
	if arg["after_open"] !="" {
       this.After_Open=arg["after_open"]
	}
	if arg["title"] !="" {
		this.Title=arg["title"]
	}
	if arg["text"] !="" {
		this.Title=arg["text"]
	}
	if arg["image"] !="" {
		this.Image=arg["image"]
	}
	if arg["jumptype"] !="" {
		n,err:= strconv.Atoi(arg["jumptype"])
		if err !=nil {
			this.JumpType=n
		}
	}
	if arg["messagetype"] !="" {
		n,err:= strconv.Atoi(arg["messagetype"])
		if err !=nil {
			this.MessageType=n
		}
	}
	if arg["newsid"] !="" {
		n,err:= strconv.Atoi(arg["newsid"])
		if err !=nil {
			this.NewsId=n
		}
	}
	if arg["url"] !="" {

		this.Url=arg["url"]
	}
	if arg["expire_time"] !="" {

		this.ExpireTime=arg["expire_time"]
	}
	if arg["device_tokens"] !="" {

		this.ExpireTime=arg["device_tokens"]
	}
	return this

}