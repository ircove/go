package repository

import (
	."database/sql"
	. "ircove.com/common"
	"errors"
	"fmt"
)

var connstring string

func init() {
	connstring = Conf.Read_String("database.connstring")
}

func DbConnecton() (*DB, error)  {
	 return  Open("sqlserver", connstring)
}

func Db_Query(cmd string, callback func(rows *Rows) error, paramters ...interface{}) error {
	conn, err := Open("sqlserver", connstring)
	if err != nil {
		return errors.New(fmt.Sprintf("数据库连接失败[%s]", err.Error()))
	}
	defer conn.Close()
	stmt, err := conn.Prepare(cmd)
	if err != nil {
		return err
	}
	rows, err := stmt.Query(paramters...)
	if err != nil {
		return err
	}
	for rows.Next() {
		err = callback(rows)
		if err != nil {
			return err
		}
	}
	return nil
}
