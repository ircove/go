package repository

import (
	"database/sql"
	"fmt"
	"log"
)

func test()  {
	connString := ""
	conn, err := sql.Open("sqlserver", connString)
	if err != nil {
		log.Fatal("数据库连接失败:", err.Error())

	}
	defer conn.Close()
	stmt, err := conn.Prepare("select top (@top) [id],[title],[content]from [vfweme].[dbo].[vf_appnews] where id>@id order by id")
	if err != nil {
		log.Fatal("Prepare failed:", err.Error())

	}
	defer stmt.Close()
	rows ,err:= stmt.Query(sql.Named("top",10),sql.Named("id",18000),sql.Named("title","aa"))
	if err != nil {
		log.Fatal("stmt Query:", err.Error())
	}
	for ;rows.Next(); {
		var newsid     int
		var title  string
		var content    string
		err = rows.Scan(&newsid,&title,&content)
		if err !=nil{
			log.Fatal("数据转换实体错误", err.Error())
			continue
		}
		fmt.Printf(" id{%d} tilte{%s} content:{%s} \n ",newsid,title,content)
	}
}
