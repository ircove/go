package services

import (
	"ircove.com/irc"
	"ircove.com/repository"
	"strconv"
)

func redis_userAuth(context *irc.VFContext) {
	db := repository.RedisConnection()

	parmeters:=[]string{
		context.Client.Server.ServerName,
		strconv.FormatUint(context.Client.Id,10),
		context.Client.Session.DeviceId,
		strconv.Itoa(context.Client.Session.AppId),
		strconv.FormatInt(context.Client.Session.UserID,10),
	}
	repository.LuaScript("UserAuth").Run(db,parmeters )

}
